    //查询信息
    var sBtn = document.querySelector('#sBtn');//入住查询按钮
    // var sNumber = document.querySelector('#sNumber');//入住房号查询
    var sType = document.querySelector('#sType');//入住房间类型查询
    var sState = document.querySelector('#sState');//入住房间状态查询
    var sTime = document.querySelector('#sTime');//入住时间查询
    //更改弹窗中的数据信息
    var u_rNum = document.querySelector('.update_roomNumber');//房间号更换
    var u_sTime = document.querySelector('.update_startTime');//入住时间更换
    var u_eTime = document.querySelector('.update_endTime');//结束时间更换
    var u_uName = document.querySelector('.update_userName');//入住用户姓名更改
    var u_uId = document.querySelectorAll('.update_userId');//入住用户身份证号码更改
    var u_uTel = document.querySelector('.update_userTelphone');//入住用户电话更改
    //延期弹窗中的数据信息
    var d_rNum = document.querySelector('.delay_roomNumber');
    var d_sTime = document.querySelector('.delay_startTime');
    var d_eTime = document.querySelector('.delay_endTime');
    var d_uName = document.querySelector('.delay_userName');
    var d_uId = document.querySelectorAll('.delay_userId');
    var d_uTel = document.querySelector('.delay_userTelphone');
    //入住弹窗中的数据信息
    var occ_rNum = document.querySelector('.occupany_roomNumber');
    var occ_sTime = document.querySelector('.occupany_startTime');
    var occ_eTime = document.querySelector('.occupany_endTime');
    var occ_uName = document.querySelector('.occupany_userName');
    var occ_uId = document.querySelectorAll('.occupany_userId');
    var occ_uTel = document.querySelector('.occupany_userTelphone');
    //预定弹窗中的数据信息
    var res_rNum = document.querySelector('.reserve_roomNumber');
    var res_sTime = document.querySelector('.reserve_startTime');
    var res_eTime = document.querySelector('.reserve_endTime');
    var res_uName = document.querySelector('.reserve_userName');
    var res_uId = document.querySelectorAll('.reserve_userId');
    var res_uTel = document.querySelector('.reserve_userTelphone');
    //取消预定中的数据信息
    // var  can_rNum = document.querySelector('.roomNumber');
    var  can_rState = document.querySelector('.roomState');
    var  can_rIsuse = document.querySelector('.roomIsuse');
    //操作按钮--弹窗
    var occBtn = document.querySelector('.occupanyBtn');
    var uBtn = document.querySelector('.updateBtn');
    var dBtn = document.querySelector('.delayBtn');
    var rBtn = document.querySelector('.reserveBtn');
    var cBtn = document.querySelector('.cancelBtn');
    // var okBtn = document.querySelectorAll('.okBtn');
    var res_occBtn = document.querySelector('.res_occBtn');
    //tbody
    var tbody = document.querySelector('#tbody');
    var tbody1 = document.querySelector('#tbody1');
    var tbody2 = document.querySelector('#tbody2');
    var tbody3 = document.querySelector('#tbody3');
    var tbody4 = document.querySelector('#tbody4');
    // var tbody5 = document.querySelector('#tbody5');
    //房间表
    var r_number = document.querySelector('#number');
    var r_type = document.querySelector('#type');//房间名
    var r_state = document.querySelector('#state');//房间状态
    // var r_ope = document.querySelector('#operation');

    var page = 1;
    var laypage;
    var num = 5;

    //更改入住信息
    tbody1.addEventListener('click',function (ev) {
        var uParent = ev.target.parentNode.parentNode;
        if (ev.target.className == 'layui-btn layui-btn-warm uBtn'){
            // tId = ev.target.parentNode.parentNode.dataset.id;
            u_rNum.value = uParent.children[0].innerHTML;
            u_sTime.value = uParent.children[1].innerHTML;
            u_eTime.value = uParent.children[2].innerHTML;
            u_uName.value = uParent.children[3].innerHTML;
            u_uId.value = uParent.children[4].innerHTML;
            u_uTel.value = uParent.children[5].innerHTML;
            layer.open({
                type:1,
                btn:['确定','取消'],
                yes:function (index,layero) {
                    $ajax({
                        url:'/api/hotelRoomAndGusestMangement/ExchangeRoom',
                        type:'get',
                        data:{
                            rn_num:u_rNum.value,
                            rd_startTime:u_sTime.value,
                            rd_endTime:u_eTime.value,
                            rd_userName1:u_uName.value,
                            rd_userId1:u_uId,
                            rd_tel:u_uTel,
                        },
                        success:function (response) {
                            if(response.code == 200){
                                getContent();
                                alert("更改成功！");

                            }
                            if(response.code == 501){
                                alert('更改失败');
                            }else if(response.code == 500){
                                alert('系统发生未知错误，请稍后再试！');
                            }
                        }
                    });
                    layer.close(index);
                }
            })

        }
    })
    //延期
    tbody2.addEventListener('click',function (ev){
        var dParent = ev.target.parentNode.parentNode;
        if (ev.target.className == 'layui-btn layui-btn-warm dBtn'){
            // tId = ev.target.parentNode.parentNode.dataset.id;
            d_rNum.value = dParent.children[0].innerHTML;
            d_sTime.value = dParent.children[1].innerHTML;
            d_eTime.value = dParent.children[2].innerHTML;
            d_uName.value = dParent.children[3].innerHTML;
            d_uId.value = dParent.children[4].innerHTML;
            d_uTel.value = dParent.children[5].innerHTML;
            $ajax({
                url:'/api/hotelRoomAndGusestMangement/DelayRoomTime',
                type:'get',
                data:{
                    rn_num:d_rNum.value,
                    rd_endTime:d_eTime.value,
                },
                success:function (response) {
                    if(response.code == 200){
                        getContent();
                        alert("更改日期成功！");

                    }
                    if(response.code == 501){
                        alert('更改日期失败，请重新选择日期');
                    }else if(response.code == 500){
                        alert('系统发生未知错误，请稍后再试！');
                    }
                }
            });
        }
    })
    //入住
    tbody3.addEventListener('click',function (ev) {
        var occParent = ev.target.parentNode.parentNode;
        if (ev.target.className == 'layui-btn layui-btn-warm occBtn'){
            // tId = ev.target.parentNode.parentNode.dataset.id;
            occ_rNum.value = occParent.children[0].innerHTML;
            occ_sTime.value = occParent.children[1].innerHTML;
            occ_eTime.value = occParent.children[2].innerHTML;
            occ_uName.value = occParent.children[3].innerHTML;
            occ_uId.value = occParent.children[4].innerHTML;
            occ_uTel.value = occParent.children[5].innerHTML;
            $ajax({
                url:'/api/hotelRoomAndGusestMangement/Live',
                type:'get',
                data:{
                    rn_num:occ_rNum.value,
                    rd_startTime:occ_sTime.value,
                    rd_endTime:occ_eTime.value,
                    rd_userName1:occ_uName.value,
                    rd_userId1:occ_uId,
                    rd_tel:occ_uTel,
                },
                success:function (response) {
                    if(response.code == 200){
                        getContent();
                        console.log("入住信息添加成功！");

                    }
                    if(response.code == 501){
                        alert('添加入住信息失败');
                    }else if(response.code == 500){
                        alert('系统发生未知错误，请稍后再试！');
                    }
                }
            });
        }
    })
    //预定
    tbody4.addEventListener('click',function (ev) {
        var resParent = ev.target.parentNode.parentNode;
        if (ev.target.className == 'layui-btn layui-btn-warm rBtn'){
            res_rNum.value = resParent.children[0].innerHTML;
            res_sTime.value = resParent.children[1].innerHTML;
            res_eTime.value = resParent.children[2].innerHTML;
            res_uName.value = resParent.children[3].innerHTML;
            res_uId.value = resParent.children[4].innerHTML;
            res_uTel.value = resParent.children[5].innerHTML;
            $ajax({
                 url:'/api/hotelRoomAndGusestMangement/BookRoom',
                 type:'get',
                data:{
                    rn_num:res_rNum.value,
                    rd_startTime:res_sTime.value,
                    rd_endTime:res_eTime.value,
                    rd_userName1:res_uName.value,
                    rd_userId1:res_uId,
                    rd_tel:res_uTel,
                },
                success:function (response) {
                    if(response.code == 200){
                        getContent();
                        alert("预定成功！");

                    }
                    if(response.code == 501){
                        alert('预定失败');
                    }else if(response.code == 500){
                        alert('系统发生未知错误，请稍后再试！');
                    }
                }
            });
        }
    })
    // 状态为‘已预定’的入住按钮、取消
    tbody.addEventListener('click',function (ev) {
        var canParent = ev.target.parentNode.parentNode;
        if(ev.target.className == 'layui-btn layui-btn-warm res_occBtn'){
            sState.value = canParent.children[1].innerHTML;
        }
        if (ev.target.className == 'layui-btn layui-btn-warm cBtn'){
            can_rState.value = canParent.children[1].innerHTML;
            can_rIsuse.value = canParent.children[2].innerHTML;
            $ajax({
                url:'/api/hotelRoomAndGusestMangement/CancelRoom',
                type:'get',
                data:{
                    rn_state:can_rState.value,
                    rn_isuse: can_rIsuse.value,
                },
                success:function (res) {
                    if(res.error == 0){
                        alert('已取消');
                    }
                }
            });
        }
    })
    // 获取后台表格数据
    function getContent(){
        console.log(1);
        $.ajax({
            url:'/api/hotelRoomAndGusestMangement/MainQuery',
            data:{
                rn_id: r_number.value,
                rt_name:r_type.value,
                rn_isuse:r_state.value,
                startTime:sTime.value,
                page:page,
                num:num,
            },
            success:function (res) {
                if(res.error == 0){
                    console.log(res);
                    tbody.innerHTML ='';
                    tbody1.innerHTML = '';
                    tbody2.innerHTML = '';
                    tbody3.innerHTML = '';
                    tbody4.innerHTML = '';

                    for (var i = 0; i < res.data.length; i++){
                        var addtr = document.createElement('tr');
                        var addInput1 = document.createElement('input');
                        var addInput2 = document.createElement('input');
                        var addInput3 = document.createElement('input');
                        var addInput4 = document.createElement('input');

                        addtr.dataset.id = res.data[i].rn_id;
                        addInput1.dataset.id = res.data[i].rd_id;
                        addInput2.dataset.id = res.data[i].rd_id;
                        addInput3.dataset.id = res.data[i].rd_id;
                        addInput4.dataset.id = res.data[i].rd_id;

                        if(res.data[i].rn_isuse=='1'){
                            var str = '<td>${res.data[i].rn_num}</td>' +
                                '<td>${res.data[i].rt_name}</td>' +
                                '<td>${res.data[i].rn_isuse}</td>' +
                                '<td><button type="button" class="layui-btn layui-btn-warm updateBtn">更换</button>' +
                                '<button type="button" class="layui-btn layui-btn-warm delayBtn">延期</button>' +
                                '</td>';
                            // addInput1 tbody1--更改
                            var update_str = '<input type="text">${res.data[i].rn_num' +
                                '<input type="text">${res.data[i].rd_startTime' +
                                '<input type="text">${res.data[i].rd_endTime' +
                                '<p>入住人员信息:</p>' +
                                '<input type="text">${res.data[i].rd_userName1' +
                                '<input type="text">${res.data[i].rd_userId1' +
                                '<input type="text">${res.data[i].rd_tel';
                            //addInput1 tbody2--延期
                            var delay_str = '<input type="text">${res.data[i].rn_num' +
                                '<input type="text">${res.data[i].rd_startTime' +
                                '<input type="text">${res.data[i].rd_endTime' +
                                '<p>入住人员信息:</p>'
                                '<input type="text">${res.data[i].rd_userName1' +
                                '<input type="text">${res.data[i].rd_userId1' +
                                '<input type="text">${res.data[i].rd_tel';

                        }else if(res.data[i].rn_isuse=='0'){
                            var str='<td>${res.data[i].rn_num}</td>' +
                                '<td>${res.data[i].rt_name}</td>' +
                                '<td>${res.data[i].rn_isuse}</td>' +
                                '<td><button type="button" class="layui-btn layui-btn-warm occupanyBtn">入住</button>' +
                                '<button type="button" class="layui-btn layui-btn-warm reserveBtn">预定</button>' +
                                '</td>';
                            // addInput1 tbody3--入住登记
                            var occ_str = '<input type="text">' +
                                '<input type="text">' +
                                '<input type="text">' +
                                '<p>入住人员信息:</p>' +
                                '<input type="text">' +
                                '<input type="text">' +
                                '<input type="text">';
                            // addInput1 tbody4--预定
                            var res_str = '<input type="text">${res.data[i].rn_num' +
                                '<input type="text">' +
                                '<input type="text">' +
                                '<p>入住人员信息:</p>' +
                                '<input type="text">' +
                                '<input type="text">' +
                                '<input type="text">';

                        }else if(res.data[i].rn_isuse=='2'){
                            var str= '<td class="roomNumber">${res.data[i].rn_num}</td>' +
                                '<td class="roomState">${res.data[i].rt_name}</td>' +
                                '<td class="roomIsuse">${res.data[i].rn_isuse}</td>' +
                                '<td><button type="button" class="layui-btn layui-btn-warm res_occBtn">入住</button>' +
                                '<button type="button" class="layui-btn layui-btn-warm cancelBtn">取消</button>' +
                                '</td>';
                        }

                        addtr.innerHTML = str;
                        addInput1.innerHTML = update_str;
                        addInput2.innerHTML = delay_str;
                        addInput3.innerHTML = occ_str;
                        addInput4.innerHTML = res_str;

                        tbody.appendChild(addtr);
                        tbody1.appendChild(addInput1);
                        tbody2.appendChild(addInput2);
                        tbody3.appendChild(addInput3);
                        tbody4.appendChild(addInput4);
                    }
                    //分页
                    layui.use('laypage',function () {

                        laypage = layui.laypage;
                        laypage.render({
                            elem:'pageNum',
                            count:res.total,
                            curr:page,
                            jump:function (obj, first) {
                                //obj包含了当前分页的所有参数，比如：
                                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                page = obj.curr;
                                // getCon();
                                console.log(first);
                                //首次不执行
                                if (!first) {
                                    //do something
                                    getCon();
                                }
                            }
                        })
                    });
                }
            }
        });
    }

    //刷新接受数据
    getContent();
    //查询
    sBtn.onclick = function () {
        getContent();
    }
   //更改入住信息
    uBtn.onclick = function () {
        getContent();
    }
    //延期入住信息
    dBtn.onclick = function () {
        getContent();
    }
    //入住信息登记
    occBtn.onclick = function () {
        getContent();
    }
    //预定房间信息登记
    rBtn.onclick = function () {
        getContent();
    }
    //取消预定，房间状态变为空闲
    cBtn.onclick = function(){
        can_rState.innerHTML = '空闲';
        can_rIsuse.innerHTML = '可用';
    }
    //状态为‘已预定’的入住按钮
    res_occBtn.onclick = function(){
        sState.innerHTML = '已入住';
    }
    //分页
    layui.use('laypage',function () {

         laypage = layui.laypage;
        laypage.render({
            elem:'pageNum',
            count:50,
        })
    });




//childrenWindows.js
    //操作按钮：入住、更改、延期、预定
    var open_oBtn = document.querySelectorAll('.occupanyBtn');
    var open_uBtn = document.querySelectorAll('.updateBtn');
    var open_dBtn = document.querySelectorAll('.delayBtn');
    var open_rBtn = document.querySelectorAll('.reserveBtn');
    // var open_cBtn = document.querySelector('.cancelBtn');

    //弹窗上的按钮
    var cBtn = document.querySelectorAll('.closeBtn');
    var okBtn = document.querySelectorAll('.okBtn');

    //弹出框
    var open_bg = document.querySelector('.open_background');
    var open_delay_background = document.querySelector('.open_delay_background');
    var open_update_background = document.querySelector('.open_update_background');
    var open_reserve_background = document.querySelector('.open_reserve_background');


    //入住,更换，延期、预定
    for (i=0;i<open_oBtn.length;i++){
        open_oBtn[i].onclick = function show() {
            open_bg.style.display = "block";
        };
    }
    for (i=0;i<open_uBtn.length;i++){
        open_uBtn[i].onclick = function show() {
            open_update_background.style.display = "block";
        };
    }
    for (i=0;i<open_dBtn.length;i++){
        open_dBtn[i].onclick = function show() {
            open_delay_background.style.display = "block";
        };
    }
    for (i=0;i< open_rBtn.length;i++){
        open_rBtn[i].onclick = function show() {
            open_reserve_background.style.display = "block";
        };
    }
    for (i=0;i< cBtn.length;i++){//关闭弹出框
        cBtn[i].onclick = function show() {
            open_bg.style.display = "none";
            open_delay_background.style.display = "none";
            open_update_background.style.display = "none";
            open_reserve_background.style.display = "none";
        };
    }