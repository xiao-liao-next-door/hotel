//JavaScript代码区域
layui.use('element', function(){
    var element = layui.element;
});
// 弹层
layui.use('layer', function(){
    var layer = layui.layer;
});
// 表单样式
layui.use('form', function(){
    var form = layui.form;
});

var laypage;
var oAddBtn=document.querySelector('.addBtn');
var oAddBox=document.querySelector('#addBox');
var oEditBox=document.querySelector('#editBox');
var tbody=document.querySelector('tbody');
var oSearchBtn=document.querySelector('.searchBtn');
var oSearchInp=document.querySelector('.searchInp');
var page=1;
// 每页显示记录数
var num=5;
var id;

var oaddRoomName=document.querySelector(".addRoomName");
var oaddRoomPrice=document.querySelector(".addRoomPrice");
var oaddRoomMsg=document.querySelector(".addRoomMsg");

var editName=document.querySelector('.editName');
var editPrice=document.querySelector('.editPrice');
var editMsg=document.querySelector('.editMsg');

var oEditBtn=document.querySelectorAll('.editBtn');
var oDelBtn=document.querySelectorAll('.delBtn');

// 分页
layui.use('laypage',function () {
    var laypage = layui.laypage;
    laypage.render({
        elem: 'pageBar',
        count: 50
    });
});

// 删除房型
for(var i=0;i<oDelBtn.length;i++){
    oDelBtn[i].onclick=function(){
        layer.open({
            type: 0,
            title: '删除房型',
            content:'确定要删除此房型吗？',
            btn:['确定','取消'],
            yes:function (index,layero) {
                alert(1);
                layer.close(index);
            }
        });
    };
};
//新增
oAddBtn.onclick=function () {
    layer.open({
        type:1,
        title:'新增房型',
        area:['600px','360px'],
        content:$(oAddBox),
        btn:['确定','取消'],
        yes:function (index,layero) {
            // $.ajax({
            //     url:'/api/user/guest',
            //     type:'get',
            //     data:{
            //         rt_name:oaddRoomName.value,
            //         rt_price:oaddRoomPrice.value,
            //         rt_msg:oaddRoomMsg.value
            //     },
            //     success:function (res) {
            //         if(res.error==0){
            //             getCon();
            //         }
            //     }
            // });
            alert("新增成功!");
            layer.close(index);
        }
    });
};
// 编辑
for(var i=0;i<oEditBtn.length;i++){
    oEditBtn[i].onclick=function() {
        layer.open({
            type:1,
            title:'新增房型',
            area:['600px','360px'],
            content:$(oEditBox),
            btn:['确定','取消'],
            yes:function (index,layero) {
                alert(1);
                layer.close(index);
            }
        });
    };
};

//获取数据的值
// function Inner(res) {
//     tbody.innerHTML='';
//     for(var i=0;i<res.data.length;i++){
//         //父级.appendChild(子级) 向父级的最后面添加子级
//         var otr=document.createElement('tr');
//         otr.dataset.id=res.data[i].rt_Id;
//         otr.innerHTML=`<td><input type="checkbox"></td>
//                         <td>${res.data[i].rt_Name}</td>
//                         <td>${res.data[i].rt_Price}</td>
//                         <td>${res.data[i].rt_Msg}</td>
//                         <td class="txt-color">
//                             <span><button type="button" class="layui-btn editBtn">编辑</button></span>
//                             <span><button type="button" class="layui-btn delBtn">删除</button></span>
//                         </td>`
//         tbody.appendChild(otr);
//     }
// };

//获取数据
// function getCon()   {
//     $.ajax({
//         url:'/apn/roomNumber/updatePro',
//         type:'post',
//         data:{
//             page:page,
//             num:num
//         },
//         success:function (res) {
//             console.log(res);
//             Inner(res);
//
//             layui.use('laypage', function(){
//                 laypage = layui.laypage;
//                 laypage.render({
//                     elem: 'pageBar',
//                     limit: num,
//                     count: res.number,
//                     jump: function(obj, first){
//                         //首次不执行
//                         if(!first){
//                             //do something
//                         }
//                         $.ajax({
//                             url:'/api/user/guestoffer',
//                             type:'post',
//                             data:{
//                                 page:obj.curr,
//                                 num:obj.limit
//                             },
//                             success:function (res) {
//                                 Inner(res);
//                             }
//                         });
//                     }
//                 });
//             });
//         }
//     })
// }

// 一上来就要发送ajax获取第一页的数据
// getCon();
