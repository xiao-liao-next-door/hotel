//JavaScript代码区域
layui.use('element', function(){
    var element = layui.element;
});
// 弹层
layui.use('layer', function(){
    var layer = layui.layer;
});


var laypage;
var oAddBtn=document.querySelector('.addBtn');
var oAddBox=document.querySelector('#addBox');
var oEditBox=document.querySelector('#editBox');
var tbody=document.querySelector('tbody');
var oSearchBtn=document.querySelector('.searchBtn');
var oSearchInp=document.querySelector('.searchInp');
var page=1;
// 每页显示记录数
var num=5;
var id;
// var oToken='eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1NjFiMmNkMC1jNzVhLTQ4N2MtYjYwYi0zOGYyZGI2OGIzMmEiLCJpc3MiOiJHWEEiLCJzdWIiOiJ7XCJ1X0lkXCI6MSxcInVfTmFtZVwiOlwiYWRtaW5cIixcInBfTmFtZVwiOm51bGwsXCJwX1Bvd2VyXCI6bnVsbCxcInVfVGVsXCI6XCIxMjM0NTY3ODkxMFwiLFwicF9JZFwiOjAsXCJ1X1B3ZFwiOlwiRTEwQURDMzk0OUJBNTlBQkJFNTZFMDU3RjIwRjg4M0VcIixcInVfSXNVc2VcIjoxLFwidV9TZXhcIjpcIueUt1wiLFwidV9QX0lkXCI6MX0iLCJpYXQiOjE2MTE5OTY2NjEsImV4cCI6MTYxMTk5ODQ2MX0.m8fMXx4EaD4aXcQZ-OG36ljs8Ntnn_OTRo6eTSpx0kI';

var oaddRoomName=document.querySelector(".addRoomName");
var oaddRoomPrice=document.querySelector(".addRoomPrice");
var oaddRoomMsg=document.querySelector(".addRoomMsg");

var editName=document.querySelector('.editName');
var editPrice=document.querySelector('.editPrice');
var editMsg=document.querySelector('.editMsg');

// 删除房型
tbody.addEventListener('click',function (ev) {
    id=ev.target.parentNode.parentNode.parentNode.dataset.id;
    if(ev.target.className=='layui-btn delBtn'){
        layer.open({
            type: 0,
            title: '删除房型',
            content:'确定要删除此房型吗？',
            btn:['确定','取消'],
            yes:function (index, layero) {
                var otr=document.createElement('tr');
                $.ajax({
                    url:'/api/user/guestdel',
                    type:'post',
                    data:{
                        rt_id:id,
                        // token:oToken
                    },
                    success:function (res) {
                        console.log("删除成功");
                        if(res.error==0){
                            getCon();
                        }
                    }
                });
                layer.close(index);
            }
        });
    }
})
//新增
oAddBtn.onclick=function () {
    layer.open({
        type:1,
        title:'新增房型',
        area:['600px','360px'],
        content:$(oAddBox),
        btn:['确定','取消'],
        yes:function (index,layero) {
            $.ajax({
                url:'/api/user/guest',
                type:'get',
                data:{
                    rt_name:oaddRoomName.value,
                    rt_price:oaddRoomPrice.value,
                    rt_msg:oaddRoomMsg.value,
                    // token:oToken
                },
                success:function (res) {
                    if(res.error==0){
                        getCon();
                    }
                }
            });
            layer.close(index);
        }
    });
    getCon();
};
// 编辑
tbody.addEventListener('click',function (ev) {
    var oParent=ev.target.parentNode.parentNode.parentNode
    //ev叫做js事件对象，储存了事件触发时候的详细信息
    //ev.target  真正触发这个事件的对象  事件源
    if(ev.target.className=='layui-btn editBtn'){
        id=ev.target.parentNode.parentNode.parentNode.dataset.id;
        editName.value=oParent.children[1].innerHTML;
        editPrice.value=oParent.children[2].innerHTML;
        editMsg.value=oParent.children[3].innerHTML;
        layer.open({
            type: 1,
            title: '编辑房型',
            area : ['600px' , '360px'],
            content:$(oEditBox),
            btn:['确定','取消'],
            yes:function (index, layero) {
                $.ajax({
                    url:'/api/user/guestupdate',
                    type:'get',
                    data:{
                        rt_id:id,
                        rt_name:editName.value,
                        rt_price:editPrice.value,
                        rt_msg:editMsg.value
                    },
                    success:function (res) {
                        if(res.error==0){
                            getCon();
                        }
                    }
                })
                layer.close(index);
            }
        });
    }
});

//获取数据的值
function Inner(res) {
    tbody.innerHTML='';
    for(var i=0;i<res.data.length;i++){
        //父级.appendChild(子级) 向父级的最后面添加子级
        var otr=document.createElement('tr');
        otr.dataset.id=res.data[i].rt_Id;
        otr.innerHTML=`<td><input type="checkbox"></td>
                        <td>${res.data[i].rt_Name}</td>
                        <td>${res.data[i].rt_Price}</td>
                        <td>${res.data[i].rt_Msg}</td>
                        <td class="txt-color">
                            <span><button type="button" class="layui-btn editBtn">编辑</button></span>
                            <span><button type="button" class="layui-btn delBtn">删除</button></span>
                        </td>`
        tbody.appendChild(otr);
    }
};

//获取数据
function getCon()   {
    $.ajax({
        url:'/api/user/guestoffer',
        type:'post',
        data:{
            page:page,
            num:num,
            // token:oToken
        },
        success:function (res) {
            console.log(res);
            Inner(res);
            layui.use('laypage', function(){
                laypage = layui.laypage;
                laypage.render({
                    elem: 'pageBar',
                    limit: num,
                    count: res.number,
                    jump: function(obj, first){
                        //首次不执行
                        if(!first){
                            //do something
                        }
                        $.ajax({
                            url:'/api/user/guestoffer',
                            type:'post',
                            data:{
                                page:obj.curr,
                                num:obj.limit,
                                token:oToken
                            },
                            success:function (res) {
                                Inner(res);
                            }
                        });
                    }
                });
            });
        },
    })
}

//一上来就要发送ajax获取第一页的数据
getCon();
// 搜索房间
oSearchBtn.onclick=function () {
    $.ajax({
        url:'/api/user/guestquery',
        type:'get',
        data:{
            rt_name:oSearchInp.value,
            // token:oToken
        },
        success:function (res) {
            Inner(res);
            console.log("搜索成功!");
        }
    })
};

